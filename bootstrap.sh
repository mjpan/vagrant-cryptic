#!/usr/bin/env bash

apt-get update

apt-get install -y git
apt-get install -y emacs23

apt-get install -y python-pip
pip install --upgrade pip

pip install virtualenv
pip install tornado
pip install pyasn1
pip install pycrypto
pip install python-keyczar
pip install nose
pip install requests